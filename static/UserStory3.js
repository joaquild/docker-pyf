function loadEvents() {
    document.getElementById("registrarButton").addEventListener('click', postUserJSON);
}
function alertOk(){
    alert("Se ha creado el usuario correctamente");
}
function sendData() {
    user_id = document.getElementById("inputUsername").value;
    password = document.getElementById("inputPassword").value;

    data = JSON.stringify({ "user_id": user_id, "password": password });
    alertOk();
    return data;

}

function postUserJSON() {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:5000/user", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(sendData());
}